<?php

print <<<HEADER
<!DOCTYPE html>

<html lang="en">

<head>
    <title>Peace of Mind</title>
    <meta charset="utf-8">
    <meta name="description" content="Mental Health Repository">
    <meta name="author" content="Florencia Froebel">
    <link href="signin.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="menu.js"></script>
    <script src="index.js"></script>
    <link rel="icon" type="image/png" href="logo.jpeg">

    <style>
        table, th, tr, td{
            border: 1px solid black;
        }
    </style>
</head>
<body>

<section id="container">

    <!-- Header -->
    <header id="header"> 
        <div><a href="index.html"><img class="headerPic" id="leftpic" src="logo.jpeg" alt="Logo Goes Here"></a></div>
        <div><a href="index.html" id="headerLink"><h1>Peace of Mind</h1></a></div>
        <div><a href="index.html"><img class="headerPic" id="rightpic" src="logo.jpeg" alt="Logo Goes Here"></a></div>
    </header>

    <!-- Main Body -->
    <section id="mid">
    <!-- Navigation Bar -->
    <section id="nav">
        <div class="navigation" id="resources"><a href="resources.html" class ="menuLink">Resources </a>
            <div class="submenu">
                <div class="submenuLink"><a href="resources.html#Emergency">Emergency Resources</a></div>
                <div class="submenuLink"><a href="resources.html#otherResources">Local & National Resources</a></div>
                <div class="submenuLink"><a href="signin.php">Suggest a New Resource</a></div>
            </div></div>
        <div class="navigation"><a href="information.html" class ="menuLink">Information</a>
            <div class="submenu">
                <div class="submenuLink"><a href="anxiety.html">Anxiety</a></div>
                <div class="submenuLink"><a href="OCD.html">OCD</a></div>
                <div class="submenuLink"><a href="ED.html">Eating Disorders</a></div>
                <div class="submenuLink"><a href="depression.html">Depression</a></div>
                <div class="submenuLink"><a href="insomnia.html">Insomnia</a></div>
                <div class="submenuLink"><a href="adhd.html">ADHD</a></div>
            </div></div>
        <div class="navigation"><a href="getInvolved.html" class ="menuLink">Get Involved</a>
            <div class="submenu">
                <div class="submenuLink"><a href="getInvolved.html#awareness">Spread Awareness</a></div>
                <div class="submenuLink"><a href="getInvolved.html#educated">Get Educated</a></div>
                <div class="submenuLink"><a href="getInvolved.html#conversation">Start A Conversation</a></div>
                <div class="submenuLink"><a href="getInvolved.html#donate">Donate</a></div>
                <div class="submenuLink"><a href="getInvolved.html#act">Act</a></div>
            </div></div>
        <div class="navigation"><a href="ContactUs.php" class ="menuLink">Contact Us</a>
            <div class="submenu">
                <div class="submenuLink"><a href="ContactUs.php#authors">About The Authors</a></div>
                <div class="submenuLink"><a href="ContactUs.php#feedback">Feedback Form</a></div>
                <div class="submenuLink"><a href="ContactUs.php#contactLinks">Contact Links</a></div>
            </div></div>
    </section>
        
    <!-- About Us -->
    <section id="signin">
HEADER;

check_db();

print <<<FOOTER
    <br /><br /><br /><br /><br /><br />
    </section>
    </section>

    <!-- footer -->
    <footer id="footer">
        <p>Sign up for our <a href="ContactUs.php#feedback">newsletter</a>!</p>
        <p id="footerSmall">
        &copy; 2020 Peace of Mind &nbsp;&nbsp;&nbsp;
        Froebel, DiLeo, Kahan &nbsp;&nbsp;&nbsp;
        <script>
            document.write(document.lastModified);
        </script>
        </p>
    </footer>
</section>
</body>
</html>
FOOTER;

function check_db() {
    // README
    // Information below is for Michael DiLeo, if you need to use your own
    // database make sure to edit the information below

    // Database information
    $server = "fall-2020.cs.utexas.edu";
    $dbUser   = "cs329e_bulko_mjdileo";
    $dbPwd    = "rinse-method*desire";
    $dbName = "cs329e_bulko_mjdileo";
    $tableNamePass = "projectPasswd";
    $tableNameSuggest = "projectSuggestions";
    $tableNameFeedback = "projectFeedback";

    // Connect to MySQL Server
    $mysqli = new mysqli($server, $dbUser, $dbPwd, $dbName);
    if ($mysqli->connect_errno) {
       die('Connect Error: ' . $mysqli->connect_errno . ": " .  $mysqli->connect_error);
    }
    // Select Database
    $mysqli->select_db($dbName) or die($mysqli->error);

    // Build query
    $command = "SELECT * FROM $tableNamePass";
    // Execute query
    $result = $mysqli->query($command) or die($mysqli->error);

    // Result empty
    if($result->num_rows == 0)
    {
        echo '<p>Match Not Found</p>';
    }
    else{
        print "<h3>Users</h3>";
        print "<table align='center'>";
        print "<tr><th>Email</th><th>Password</th></tr>";
        while ($row = $result->fetch_row()) {
            echo "<tr><td>$row[0]</td><td>*******</td></tr>";
        }
        print "</table><br /><br />";
    }


    // Build query
    $command = "SELECT * FROM $tableNameSuggest";
    // Execute query
    $result = $mysqli->query($command) or die($mysqli->error);

    // Result empty
    if($result->num_rows == 0)
    {
        echo '<p>Match Not Found</p>';
    }
    else{
        print "<h3>Resource Suggestions</h3>";
        print "<table align='center'>";
        print "<tr><th>URL</th><th>description</th></tr>";
        while ($row = $result->fetch_row()) {
            echo "<tr><td>$row[0]</td><td>$row[1]</td></tr>";
        }
        print "</table><br /><br />";
    }


    // Build query
    $command = "SELECT * FROM $tableNameFeedback";
    // Execute query
    $result = $mysqli->query($command) or die($mysqli->error);

    // Result empty
    if($result->num_rows == 0)
    {
        echo '<p>Match Not Found</p>';
    }
    else{
        print "<h3>Feedback</h3>";
        print "<table align='center'>";
        print "<tr><th>ID</th><th>Name</th><th>Email</th><th>Comment</th><th>Feedback</th></tr>";
        while ($row = $result->fetch_row()) {
            echo "<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td></tr>";
        }
        print "</table>";
    }
}

?>
