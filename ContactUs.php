<!DOCTYPE html>

<html lang="en">

<head>
	<title>Peace of Mind</title>
	<meta charset="utf-8">
	<meta name="description" content="Mental Health Repository - Contact Us Page">
	<meta name="author" content="Noah Kahan">
    <link href="ContactUs.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="menu.js"></script>
    <script src="ContactUs.js"></script>
    <link rel="icon" type="image/png" href="logo.jpeg">
</head>
<body>
<section id="container">
	<!-- Header -->
    <header id="header"> 
        <div><a href="index.html"><img class="headerPic" id="leftpic" src="logo.jpeg" alt="Logo Goes Here"></a></div>
        <div><a href="index.html" id="headerLink"><h1>Peace of Mind</h1></a></div>
        <div><a href="index.html"><img class="headerPic" id="rightpic" src="logo.jpeg" alt="Logo Goes Here"></a></div>
    </header>

	<!-- Main Body -->
	<section id="mid">
	<!-- Navigation Bar -->
	<section id="nav">
		<div class="navigation" id="resources"><a href="resources.html" class ="menuLink">Resources </a>
			<div class="submenu">
				<div class="submenuLink"><a href="resources.html#Emergency">Emergency Resources</a></div>
				<div class="submenuLink"><a href="resources.html#otherResources">Local & National Resources</a></div>
                <div class="submenuLink"><a href="signin.php">Suggest a New Resource</a></div>
			</div></div>
		<div class="navigation"><a href="information.html" class ="menuLink">Information</a>
			<div class="submenu">
				<div class="submenuLink"><a href="anxiety.html">Anxiety</a></div>
				<div class="submenuLink"><a href="OCD.html">OCD</a></div>
				<div class="submenuLink"><a href="ED.html">Eating Disorders</a></div>
				<div class="submenuLink"><a href="depression.html">Depression</a></div>
				<div class="submenuLink"><a href="insomnia.html">Insomnia</a></div>
				<div class="submenuLink"><a href="adhd.html">ADHD</a></div>
			</div></div>
		<div class="navigation"><a href="getInvolved.html" class ="menuLink">Get Involved</a>
			<div class="submenu">
				<div class="submenuLink"><a href="getInvolved.html#awareness">Spread Awareness</a></div>
				<div class="submenuLink"><a href="getInvolved.html#educated">Get Educated</a></div>
				<div class="submenuLink"><a href="getInvolved.html#conversation">Start A Conversation</a></div>
				<div class="submenuLink"><a href="getInvolved.html#donate">Donate</a></div>
				<div class="submenuLink"><a href="getInvolved.html#act">Act</a></div>
			</div></div>
		<div class="navigation"><a href="ContactUs.php" class ="menuLink">Contact Us</a>
			<div class="submenu">
				<div class="submenuLink"><a href="ContactUs.php#authors">About The Authors</a></div>
				<div class="submenuLink"><a href="ContactUs.php#feedback">Feedback Form</a></div>
				<div class="submenuLink"><a href="ContactUs.php#contactLinks">Contact Links</a></div>
			</div></div>
	</section>
	
    <!-- About The Authors -->
    <div id="authors">
        <h3>About The Authors</h3>
        
        <!--Michael's Intro-->
        <div class="bio">
            <img class="photo" id="photo1" src="michael.jpg" alt="Picture of Michael" height="100" width="100">

            <p class="caption">
                Michael DiLeo - Senior undergraduate at the University of Texas at Austin pursuing a
                B.S.A in Biology, minoring in American Sign Language and earning a certificate in
                Computer Science. Born in San Antonio, Texas, Michael and his family repeatedly moved across the world as his father travelled in the Air Force. He enjoys rowing for the Texas Crew team on campus, teaching as a Sanger Learning Center tutor, and being a member of the Health Science Scholars honor's program. He is pursuing a career in 
                medicine and is currently applying and interviewing for medical school. As he is interested in medicine and health in general, he is excited to work on Peace of Mind as another way to promote healthy living and good mental health for all.
            </p>
        </div>

        <!--Florencia's Intro-->
        <div class="bio">
            <img class="photo" id="photo2" src="florencia.JPG" alt="Picture of Florencia" height="100" width="100">

            <p class="caption">
                Florencia Froebel - Senior undergraduate at the University of Texas at Austin working towards a B.S.A in Mathematics, with
				a minor in Philosophy and a certificate in Computer Science. She was born in La Paz, Bolivia and lived in Germany before
				moving to Texas. She is aspiring to start a career as a software engineer or data scientist and has a huge passion for machine
				learning. She also really enjoys playing soccer, volunteering, learning new things, and traveling. Her mother, aunt, and sister all 
				studied psychology in college and has always had the advantage of being aware of mental health and wants to spread this awareness. 
				She is very passionate about starting conversations about mental health and getting rid of the stigma behind mental illness. 
            </p>
        </div>
        
        <div class="bio">
            <!--Here is the example of how each of the different blurbs will go-->

            <!--Headshot/photo-->
            <img class="photo" id="photo3" src="noah.jpg" alt="Picture of Noah" height="100" width="100">

            <!--Description paragraph-->
            <p class="caption">
                Noah Kahan - Senior undergraduate at the University of Texas at Austin working towards a B.S. in Biochemistry and a certificate in Computer Science.
                Born in Rock Springs, WY but raised in Austin, TX. An easygoing and lighthearted bookworm, cook, and animal whisperer with passions 
                for lie in helping those that are in need through community service and teaching alongside a lifelong desire to learn.
            </p>
        </div>
    </div>

    <!-- Feedback Form -->
<?php
    
    if (!isset($_POST["submit"])) {
        makeform();
    }
    else {
        check_db();
    }
    function makeform() {
        $script = $_SERVER['PHP_SELF'];
        print <<<FEEDBACK
        <div id= "feedback">
            <hr />
            <h3>We Would Love Your Feedback!</h3>
            <form name="feedback" method="POST" action="$script">
                <div id="input">
                    <p class="label"><label for="name"> Name:  </label></p>
                    <p><input id="name" name="name" type="text"></p>
                    
                    <p class="label"><label for="email"> Email: </label></p>
                    <p><input id="email" name="email" type="email"></p>
                    
                    <p class="label"><label for="comment"> Comments: </label></p>
                    <p><textarea id="comment" name="comment" placeholder="Enter any comments here..." rows="4" cols="40"></textarea></p>

                    <p>If you would like to sign up for our weekly newsletter, please check the box below.</p>
                    <label>Yes, I would like to subscribe to the newsletter! &nbsp;&nbsp;&nbsp;<input type="checkbox" name="newsletter" value="true"></label>
                    <br />

                    <p><input class="button" type="submit" name="submit" id="submit"><input class="button" type="reset" id="reset"></p> 
                </div>
            </form>        
        </div>
FEEDBACK;
    }
    function check_db() {
        // README
        // Information below is for Michael DiLeo, if you need to use your own
        // database make sure to edit the information below

        // Database information
        $server = "fall-2020.cs.utexas.edu";
        $dbUser   = "cs329e_bulko_mjdileo";
        $dbPwd    = "rinse-method*desire";
        $dbName = "cs329e_bulko_mjdileo";
        $tableName = "projectFeedback";

        // Connect to MySQL Server
        $mysqli = new mysqli($server, $dbUser, $dbPwd, $dbName);
        if ($mysqli->connect_errno) {
           die('Connect Error: ' . $mysqli->connect_errno . ": " .  $mysqli->connect_error);
        }
        // Select Database
        $mysqli->select_db($dbName) or die($mysqli->error);

        // Retrieve data from Query String
        $name = $_POST['name'];
        $email = $_POST['email'];
        $comment = $_POST['comment'];
        if($_POST['newsletter'] == "true") {
            $newsletter = "T";
        }
        else {
            $newsletter = "F";
        }
        // Escape User Input to help prevent SQL Injection
        $name = $mysqli->real_escape_string($name);
        $email = $mysqli->real_escape_string($email);
        $comment = $mysqli->real_escape_string($comment);

        // Build query
        $command = "INSERT INTO $tableName (name, email, comment, newsletter) VALUES ('$name', '$email', '$comment', '$newsletter')";
        // Execute query
        $result = $mysqli->query($command) or die($mysqli->error);
        print <<<FEEDBACK
        <div id= "feedback">
            <hr />
            <h3>Thank you for the feedback!</h3>
            </div>
FEEDBACK;
        unset($_POST);
    }
?>

    <!-- Contact Links -->
    <hr />
    <h3> Contact Links </h3>
    <div id="contactLinks">
        <div>
            <h4>Michael:</h4>
            <a href="mailto:michael.dileo@utexas.edu">michael.dileo@utexas.edu</a>
        </div>

        <div>
            <h4>Florencia:</h4>
            <a href="mailto:florencia.froebel@gmail.com">florencia.froebel@gmail.com</a>
        </div>
        
        <div>
            <h4>Noah:</h4>
            <a href="mailto:noah.kahan@utexas.edu">noah.kahan@utexas.edu</a>
        </div>
        <br /><br /><br />
    </div>
    </section>

    <!-- footer -->
    <footer id="footer">
        <p>Sign up for our <a href="ContactUs.php#feedback">newsletter</a>!</p>
        <p id="footerSmall">
        &copy; 2020 Peace of Mind &nbsp;&nbsp;&nbsp;
        Froebel, DiLeo, Kahan &nbsp;&nbsp;&nbsp;
        <script>
            document.write(document.lastModified);
        </script>
        </p>
    </footer>
</section>

</body>
</html>
