<?php

if (!isset($_COOKIE["loggedOn"])) {
    header('Location: ./signin.php');
    exit;
}

?>

<!DOCTYPE html>

<html lang="en">

<head>
    <title>Peace of Mind</title>
    <meta charset="utf-8">
    <meta name="description" content="Mental Health Repository">
    <meta name="author" content="Florencia Froebel">
    <link href="signin.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="menu.js"></script>
    <script src="index.js"></script>
    <link rel="icon" type="image/png" href="logo.jpeg">

    <script language = "javascript" type = "text/javascript">
    //Browser Support Code
    function ajaxFunction() {
        var url = $('#url').val();
        var description = $('#description').val();
        if (url == "") {
            $('#ajaxDiv').html("Please enter a resource url");
            return null
        }
        var ajaxRequest;  // The variable that makes Ajax possible!
       
        ajaxRequest = new XMLHttpRequest();
       
        // Create a function that will receive data sent from the server and will update
        // the div section in the same page. 
        ajaxRequest.onreadystatechange = function() {
            if(ajaxRequest.readyState == 4){
                $('#ajaxDiv').html(ajaxRequest.responseText);
            }
        }
       
        // Now get the value from user and pass it to server script.
        var queryString = "?url=" + url + "&description=" + description;
       
        ajaxRequest.open("GET", "suggestion_back.php" + queryString, true);
        ajaxRequest.send(null);
    }
    </script>

</head>

<?php
    if (isset($_POST["logout"])) {
        setcookie("loggedOn", "False", time()-(60*100));
        header('Location: ./signin.php');
        exit;
    }
?>

<body>

<section id="container">

    <!-- Header -->
    <header id="header"> 
        <div><a href="index.html"><img class="headerPic" id="leftpic" src="logo.jpeg" alt="Logo Goes Here"></a></div>
        <div><a href="index.html" id="headerLink"><h1>Peace of Mind</h1></a></div>
        <div><a href="index.html"><img class="headerPic" id="rightpic" src="logo.jpeg" alt="Logo Goes Here"></a></div>
    </header>

    <!-- Main Body -->
    <section id="mid">
    <!-- Navigation Bar -->
    <section id="nav">
        <div class="navigation" id="resources"><a href="resources.html" class ="menuLink">Resources </a>
            <div class="submenu">
                <div class="submenuLink"><a href="resources.html#Emergency">Emergency Resources</a></div>
                <div class="submenuLink"><a href="resources.html#otherResources">Local & National Resources</a></div>
                <div class="submenuLink"><a href="signin.php">Suggest a New Resource</a></div>
            </div></div>
        <div class="navigation"><a href="information.html" class ="menuLink">Information</a>
            <div class="submenu">
                <div class="submenuLink"><a href="anxiety.html">Anxiety</a></div>
                <div class="submenuLink"><a href="OCD.html">OCD</a></div>
                <div class="submenuLink"><a href="ED.html">Eating Disorders</a></div>
                <div class="submenuLink"><a href="depression.html">Depression</a></div>
                <div class="submenuLink"><a href="insomnia.html">Insomnia</a></div>
                <div class="submenuLink"><a href="adhd.html">ADHD</a></div>
            </div></div>
        <div class="navigation"><a href="getInvolved.html" class ="menuLink">Get Involved</a>
            <div class="submenu">
                <div class="submenuLink"><a href="getInvolved.html#awareness">Spread Awareness</a></div>
                <div class="submenuLink"><a href="getInvolved.html#educated">Get Educated</a></div>
                <div class="submenuLink"><a href="getInvolved.html#conversation">Start A Conversation</a></div>
                <div class="submenuLink"><a href="getInvolved.html#donate">Donate</a></div>
                <div class="submenuLink"><a href="getInvolved.html#act">Act</a></div>
            </div></div>
        <div class="navigation"><a href="ContactUs.php" class ="menuLink">Contact Us</a>
            <div class="submenu">
                <div class="submenuLink"><a href="ContactUs.php#authors">About The Authors</a></div>
                <div class="submenuLink"><a href="ContactUs.php#feedback">Feedback Form</a></div>
                <div class="submenuLink"><a href="ContactUs.php#contactLinks">Contact Links</a></div>
            </div></div>
    </section>
        
    <!-- About Us -->
    <section id="signin">
    <h3>Suggest a New Resource</h3><br />
        <form method="POST">
        <label style="width:110px">Resource URL: </label><br><br><input id='url' type='text' size='60'><br /><br />
        <label style="width:150px">Resource Description: </label><br><br><textarea id="description" placeholder="Enter short resource description here..." rows="4" cols="60"></textarea><br /><br />
        <input class="button" type = "button" onclick = "ajaxFunction()" value = "Submit"/>
        <input class="button" type='reset' value='Reset'><br />
        <input class="button" type='submit' name='logout' value='Log Out'>
        </form>
        <br /><br />
        <p id="ajaxDiv"><br /></p>
        <br /><br /><br /><br /><br /><br />
    </section>
    </section>

    <!-- footer -->
    <footer id="footer">
        <p>Sign up for our <a href="ContactUs.php#feedback">newsletter</a>!</p>
        <p id="footerSmall">
        &copy; 2020 Peace of Mind &nbsp;&nbsp;&nbsp;
        Froebel, DiLeo, Kahan &nbsp;&nbsp;&nbsp;
        <script>
            document.write(document.lastModified);
        </script>
        </p>
    </footer>
</section>
</body>
</html>