//wait for html DOM to load
$(document).ready(function(){
    //event listeners for menubar hover
    $(".navigation").mouseenter(function(event){
        eventTarget = event.currentTarget;
        console.log("mouse over a nav menu")
        //display sub-menu for current hovered menu
        displaymenu(eventTarget)
    });

    //function to display the dropdown menu
    function displaymenu(menu){
        //change display value of sub-menu div
        $(menu).css("overflow","visible");
        //add in mouseout for specific menu
        $(menu).mouseleave(function(){
            hidemenu(menu)
        });
        
   
    };

    //function to hide the dropdown menu
    function hidemenu(target){
        $(target).css("overflow", "hidden")
    };
});
