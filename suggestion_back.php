<?php

error_reporting(E_ALL);
ini_set("display_errors", "on");

// README
// Information below is for Michael DiLeo database, if you need to use your own
// database make sure to edit the information below

// Database information
$server = "fall-2020.cs.utexas.edu";
$dbUser   = "cs329e_bulko_mjdileo";
$dbPwd    = "rinse-method*desire";
$dbName = "cs329e_bulko_mjdileo";
$tableName = "projectSuggestions";

// Connect to MySQL Server
$mysqli = new mysqli($server, $dbUser, $dbPwd, $dbName);
if ($mysqli->connect_errno) {
   die('Connect Error: ' . $mysqli->connect_errno . ": " .  $mysqli->connect_error);
}
// Select Database
$mysqli->select_db($dbName) or die($mysqli->error);

// Retrieve data from Query String
$url = $_GET['url'];
$description = $_GET['description'];

// Escape User Input to help prevent SQL Injection
$url = $mysqli->real_escape_string($url);
$description = $mysqli->real_escape_string($description);

// Build query
$command = "SELECT * FROM $tableName WHERE url='$url'";
// Execute query
$result = $mysqli->query($command) or die($mysqli->error);

// Result empty, url not found, enter into database
if($result->num_rows == 0) {
    $command = "INSERT INTO $tableName VALUES ('$url', '$description')";
    $result = $mysqli->query($command) or die($mysqli->error);
    print "Suggestion recorded for URL: $url";
}
// Else result is present, tell the user
else {
    print "Suggestion URL already in database";
}

?>
