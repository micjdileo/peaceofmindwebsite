<?php

print <<<HEADER
<!DOCTYPE html>

<html lang="en">

<head>
    <title>Peace of Mind</title>
    <meta charset="utf-8">
    <meta name="description" content="Mental Health Repository">
    <meta name="author" content="Florencia Froebel">
    <link href="signin.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="menu.js"></script>
    <script src="index.js"></script>
    <link rel="icon" type="image/png" href="logo.jpeg">
</head>
<body>

<section id="container">

    <!-- Header -->
    <header id="header"> 
        <div><a href="index.html"><img class="headerPic" id="leftpic" src="logo.jpeg" alt="Logo Goes Here"></a></div>
        <div><a href="index.html" id="headerLink"><h1>Peace of Mind</h1></a></div>
        <div><a href="index.html"><img class="headerPic" id="rightpic" src="logo.jpeg" alt="Logo Goes Here"></a></div>
    </header>

    <!-- Main Body -->
    <section id="mid">
    <!-- Navigation Bar -->
    <section id="nav">
        <div class="navigation" id="resources"><a href="resources.html" class ="menuLink">Resources </a>
            <div class="submenu">
                <div class="submenuLink"><a href="resources.html#Emergency">Emergency Resources</a></div>
                <div class="submenuLink"><a href="resources.html#otherResources">Local & National Resources</a></div>
                <div class="submenuLink"><a href="signin.php">Suggest a New Resource</a></div>
            </div></div>
        <div class="navigation"><a href="information.html" class ="menuLink">Information</a>
            <div class="submenu">
                <div class="submenuLink"><a href="anxiety.html">Anxiety</a></div>
                <div class="submenuLink"><a href="OCD.html">OCD</a></div>
                <div class="submenuLink"><a href="ED.html">Eating Disorders</a></div>
                <div class="submenuLink"><a href="depression.html">Depression</a></div>
                <div class="submenuLink"><a href="insomnia.html">Insomnia</a></div>
                <div class="submenuLink"><a href="adhd.html">ADHD</a></div>
            </div></div>
        <div class="navigation"><a href="getInvolved.html" class ="menuLink">Get Involved</a>
            <div class="submenu">
                <div class="submenuLink"><a href="getInvolved.html#awareness">Spread Awareness</a></div>
                <div class="submenuLink"><a href="getInvolved.html#educated">Get Educated</a></div>
                <div class="submenuLink"><a href="getInvolved.html#conversation">Start A Conversation</a></div>
                <div class="submenuLink"><a href="getInvolved.html#donate">Donate</a></div>
                <div class="submenuLink"><a href="getInvolved.html#act">Act</a></div>
            </div></div>
        <div class="navigation"><a href="ContactUs.php" class ="menuLink">Contact Us</a>
            <div class="submenu">
                <div class="submenuLink"><a href="ContactUs.php#authors">About The Authors</a></div>
                <div class="submenuLink"><a href="ContactUs.php#feedback">Feedback Form</a></div>
                <div class="submenuLink"><a href="ContactUs.php#contactLinks">Contact Links</a></div>
            </div></div>
    </section>
        
    <!-- About Us -->
    <section id="signin">
HEADER;
    
if (isset($_COOKIE["loggedOn"])) {
    header('Location: ./suggestion.php');
    exit;
}
else if (isset($_POST["signin"])) {
    check_db();
}
else {
    makeform();
}

print <<<FOOTER
    <br /><br /><br /><br /><br /><br />
    </section>
    </section>

    <!-- footer -->
    <footer id="footer">
        <p>Sign up for our <a href="ContactUs.php#feedback">newsletter</a>!</p>
        <p id="footerSmall">
        &copy; 2020 Peace of Mind &nbsp;&nbsp;&nbsp;
        Froebel, DiLeo, Kahan &nbsp;&nbsp;&nbsp;
        <script>
            document.write(document.lastModified);
        </script>
        </p>
    </footer>
</section>
</body>
</html>
FOOTER;

// Functions go here
function makeform() {
    $script = $_SERVER['PHP_SELF'];
    print <<<SIGNUP
	<h2>Sign-In</h2><br />
    <form method="POST" action="$script">
    <label>Email: </label><input name='email' type='email' size='40' required><br /><br />
    <label>Password: </label><input name='pass' type='password' size='40' required><br /><br />
    <input class='button' type='submit' name='signin' value='Log In'>
    </form>
    <br /><br />
    <a href="signin_newuser.php">Create a New Account</a><br />
    <a href="signin_reset.php">Reset Password</a>
SIGNUP;
}

function check_db() {
    // README
    // Information below is for Michael DiLeo, if you need to use your own
    // database make sure to edit the information below

    // Database information
    $server = "fall-2020.cs.utexas.edu";
    $dbUser   = "cs329e_bulko_mjdileo";
    $dbPwd    = "rinse-method*desire";
    $dbName = "cs329e_bulko_mjdileo";
    $tableName = "projectPasswd";

    // Connect to MySQL Server
    $mysqli = new mysqli($server, $dbUser, $dbPwd, $dbName);
    if ($mysqli->connect_errno) {
       die('Connect Error: ' . $mysqli->connect_errno . ": " .  $mysqli->connect_error);
    }
    // Select Database
    $mysqli->select_db($dbName) or die($mysqli->error);

    // Retrieve data from Query String
    $email = $_POST['email'];
    $pass = $_POST['pass'];
    // Escape User Input to help prevent SQL Injection
    $email = $mysqli->real_escape_string($email);
    $pass = $mysqli->real_escape_string($pass);

    // Build query
    $command = "SELECT * FROM $tableName WHERE email='$email' AND pass='$pass'";
    // Execute query
    $result = $mysqli->query($command) or die($mysqli->error);

    // Result empty, account not found
    if($result->num_rows == 0) {
        makeform();
        print "<br /><p class='warning'>Email or password incorrect, please try again</p>";
    }
    // Else result is present, sign the user in
    else {
        // Sets cookie for loggedOn and redirects
        setcookie("loggedOn", "True", time()+(60*10)); 
        header('Location: ./suggestion.php');
        exit;
    }
}

?>
